#!/bin/sh
set -x
set -e

lsblk

echo "Enter device name:"
read DEV

echo "Enter hostname:"
read HOST

VGNAME=vg-${HOST}
LVNAME=/dev/${VGNAME}/root

sgdisk --zap-all /dev/${DEV}
sgdisk -n1:0:+512M -t1:EF00 /dev/${DEV}
sgdisk -n2:0:0 -t2:8300 /dev/${DEV}

cryptsetup luksFormat /dev/${DEV}p2
cryptsetup luksOpen /dev/${DEV}p2 ${DEV}p2_crypt

pvcreate /dev/mapper/${DEV}p2_crypt
vgcreate ${VGNAME} /dev/mapper/${DEV}p2_crypt
lvcreate -l100%FREE ${VGNAME} -n root

mkfs.ext4 ${LVNAME}
mkdosfs -F 32 -n EFI /dev/${DEV}p1

mount ${LVNAME} /mnt
debootstrap --components=main,contrib,non-free,non-free-firmware --include=locales,tzdata,console-setup bookworm /mnt
mkdir /mnt/boot/efi
mount /dev/${DEV}p1 /mnt/boot/efi
for fs in dev proc sys run dev/pts sys/firmware/efi/efivars; do
  mount --rbind /${fs} /mnt/${fs}
done

echo "Apt::Install-Recommends 0;" >> /mnt/etc/apt/apt.conf.d/local-recommends
chroot /mnt apt-get -y install cryptsetup cryptsetup-initramfs lvm2 sudo systemd-boot efibootmgr network-manager wpasupplicant firmware-iwlwifi

echo PARTUUID=$(blkid -s PARTUUID -o value /dev/${DEV}p1) /boot/efi vfat nofail,x-systemd.device-timeout=1 0 1 >> /mnt/etc/fstab
echo UUID=$(blkid -s UUID -o value ${LVNAME}) / ext4 defaults 0 1 >> /mnt/etc/fstab
echo ${DEV}p2_crypt PARTUUID=$(blkid -s PARTUUID -o value /dev/${DEV}p2) none luks,discard,initramfs >> /mnt/etc/crypttab
chroot /mnt bootctl install

# set the correct root device and regenerate the kernel bootloader entry
mkdir -p /mnt/etc/kernel
echo root=UUID=$(blkid -s UUID -o value ${LVNAME}) > /mnt/etc/kernel/cmdline
chroot /mnt apt-get -y install linux-image-amd64

chroot /mnt useradd -m -G sudo -c 'Birger Schacht' birger
chroot /mnt passwd birger
