alias ip='ip -c -h'
alias grep='grep --color=auto'
alias diff='diff --color=auto'

alias bd='gbp buildpackage --git-builder=sbuild -A -v -d unstable --source-only-changes' # --git-pristine-tar-commit'
alias bde='gbp buildpackage --git-builder=sbuild -A -v -d experimental -c unstable-amd64-sbuild --source-only-changes --extra-repository="deb http://deb.debian.org/debian experimental main" --build-dep-resolver=aptitude'
alias bdn='gbp buildpackage --git-builder=sbuild -A -v -d unstable -s'

#alias cat='batcat --paging=never'
